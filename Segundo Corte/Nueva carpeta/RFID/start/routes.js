'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')
//Ruta alumnos
Route.get('alumno/alumno','AlumnoController.index')
Route.get('alumno/agregaralumno','AlumnoController.view')
Route.post('alumno/agregaralumno','AlumnoController.create')
Route.get('alumno/alumno/:id','AlumnoController.destroy')
Route.post('alumno/modificaralumno/:id','AlumnoController.update')
Route.get('alumno/modificaralumno/:id','AlumnoController.edit')

//rutas rfid
Route.get('rfi/rfi','RfiController.index')
Route.get('rfi/agregarrfi','RfiController.view')
Route.post('rfi/agregarrfi','RfiController.create')
Route.get('rfi/rfi/:id','RfiController.destroy')
Route.post('rfi/modificarrfi/:id','RfiController.update')
Route.get('rfi/modificarrfi/:id','RfiController.edit')


