'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RfiSchema extends Schema {
  up () {
    this.create('rfis', (table) => {
      table.increments('id')
      table.string('numeroRFID',45).notNullable();
      table.date('fecha').notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('rfis')
  }
}

module.exports = RfiSchema
