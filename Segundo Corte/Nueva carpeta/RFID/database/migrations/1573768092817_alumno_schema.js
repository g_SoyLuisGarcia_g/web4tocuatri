'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlumnoSchema extends Schema {
  up () {
    this.create('alumno', (table) => {
      table.increments('id')
      table.string('nombre',45).notNullable();
      table.string('apellidoP',45).notNullable();
      table.string('apellidoM',45).notNullable();
      table.integer('edad').notNullable();
      table.integer('idRFID').unsigned().references('id').inTable('rfis').onDelete('cascade').notNullable();
      table.date('fecha').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('alumno')
  }
}

module.exports = AlumnoSchema