'use strict'

const alumnos= use ('App/Models/Alumno')
const Rfi = use('App/Models/Rfi')

class RfiController {
    async index ({ request, response, view }) {
        let rfi = await Rfi.query().with('alumno').fetch();
        console.log(rfi.toJSON());
        return view.render('rfi/rfi',{rfi: rfi.toJSON()})
    }
    async show ({ params, request, response, view }) {
        let RFI = await Rfi.query().with('alumnos').fetch();
        let alumno = await alumnos.all();
        return view.render('rfi/rfi',{alumnos: alumno.toJSON(),RFI: RFI.toJSON()})
    }

    async create ({ request, response, view }) {
        const add = new Rfi();
        add.id = request.input('id');
        add.numeroRFID = request.input('numeroRFID')
        add.fecha = request.input('fecha')
        await add.save();
        return response.redirect('agregarrfi')
    }

    async view({request, response, view}){
        let rfi = await Rfi.find(params.id)
        return view.render('rfi/agregarrfi',{rfi})
    }

    /*async store ({ request, response }) {
        console.log(request.all())
        await rfi.save(request.all())
        return view.redirect('rfi/rfi')
    }*/
    async edit ({ params, request, response, view }) {
        let RFI = await Rfi.find(params.id)
        return view.render('rfi/modificarrfi',{RFI})//agregar modificarrfi
    }

    async update ({ params, request, response,view }) {
        const rfi = await Rfi.find(params.id)
        rfi.id = request.input('id');
        rfi.numeroRFID = request.input('numeroRFID')
        rfi.fecha = request.input('fecha')
        await rfi.save();
        return response.redirect('/rfi/rfi')
    }

    async destroy ({ params, request, response,view }) {
        const rfis = await Rfi.find(params.id)
        await rfis.delete();
        return response.redirect('back')
    }
}

module.exports = RfiController
