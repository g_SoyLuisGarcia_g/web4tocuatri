'use strict'
const Alumno = use('App/Models/Alumno')
const Rfi = use ('App/Models/Rfi')
class AlumnoController {
    async index ({ request, response, view }) {
        let alumnos = await Alumno.query().with('rfi').fetch();
        console.log("Alumnos-------------------------------------------");
        console.log(alumnos.toJSON());
        let rfi = await Rfi.all();
        console.log("RFI----------------------------");
        console.log(rfi.toJSON());
        return view.render('alumno/alumno',{alumnos: alumnos.toJSON(),rfi:rfi.toJSON()})
    }
    
        async create ({ request, response, view }) {
            const alumno = new Alumno();
            alumno.nombre = request.input('nombre')
            alumno.apellidoP = request.input('apellidoP')
            alumno.apellidoM = request.input('apellidoM')
            alumno.edad = request.input('edad')
            alumno.idRFID = request.input('idRFID')
            alumno.fecha = request.input('fecha')
            await alumno.save()
            return response.redirect('agregaralumno')
        }
      
  async view({request, response, view}){
    let rfi = await Rfi.query().with('alumno').fetch();
    console.log("vistallllllllll--------------")
    console.log(rfi.toJSON());
    return view.render('alumno/agregaralumno',{rfi:rfi.toJSON()})
  }

  /*async store ({ request, response }) {
    console.log(request.all());
    await Alumno.save(request.all())
    return view.redirect('alumno/alumno')
  }*/

  async show ({ params, request, response, view }) {
    let alumnos = await Alumno.query().with('Rfi').fetch();
    let rfi = await Rfi.all();
    console.log("datos------------------------------------------------");
    console.log(rfi.toJSON());
    
    return view.render('alumno',{rfi:rfi.toJSON(),alumnos:alumnos.toJSON()})
  }

  async edit ({ params, request, response, view }) {//vista
    let alumnos = await Alumno.find(params.id) 
    //console.log("id:--------"+alumnos.id);  
    return view.render('alumno/modificaralumno',{alumnos})
  }


  async update ({ params, request, response,view }) {//metodo
    const clients = await Alumno.find(params.id);
    clients.nombre = request.input('nombre')
    clients.apellidoP = request.input('apellidoP')
    clients.apellidoM = request.input('apellidoM')
    clients.edad = request.input('edad')
    clients.idRFID = request.input('idRFID')
    clients.fecha = request.input('fecha')
    await clients.save()
    //return view.render('clientes/cliente',{clients})
    return response.redirect('/alumno/alumno')
}

  async destroy ({ params, request, response,view }) {
    const alumnos = await Alumno.find(params.id);
    await alumnos.delete()
    return response.redirect('back')
  }
}

module.exports = AlumnoController
