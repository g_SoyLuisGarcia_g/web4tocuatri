'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Rfi extends Model {
  /*static get table(){
    return 'rfi';
  }*/
  rfi(){
    return this.belongsTo('App/Models/Rfi')
  }

  alumno(){
    return this.belongsTo('App/Models/Alumno','id')
  }
}

module.exports = Rfi
