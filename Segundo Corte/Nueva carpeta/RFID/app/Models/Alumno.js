'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Alumno extends Model {
    static get table(){
        return 'alumno';
    }
    rfi(){
        return this.belongsTo('App/Models/Rfi')
    }

}

module.exports = Alumno
