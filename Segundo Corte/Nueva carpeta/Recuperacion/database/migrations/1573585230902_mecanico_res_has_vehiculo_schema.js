'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MecanicoResHasVehiculoSchema extends Schema {
  up () {
    this.create('mecanico_res_has_vehiculos', (table) => {
      table.integer('mecanico responsable_idMEC').unsigned().references('id').inTable('mecanico_responsables')
      table.integer('vehiculo_idMATRICULA').unsigned().references('id').inTable('vehiculos')
      table.timestamps()
    })
  }

  down () {
    this.drop('mecanico_res_has_vehiculos')
  }
}

module.exports = MecanicoResHasVehiculoSchema
