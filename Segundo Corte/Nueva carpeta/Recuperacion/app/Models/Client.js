'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Client extends Model {
    static get table (){
        return 'clientes'
      }
      vehiculo(){
        return this.belongsTo('App/Models/Vehiculo')
      }
}

module.exports = Client