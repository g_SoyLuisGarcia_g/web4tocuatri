'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mecanicoha extends Model {
    static get table (){
        return 'mecanico_res_has_vehiculos'
      }
}

module.exports = Mecanicoha
