'use strict'
const mecanicoh = use('App/Models/Mecanicoha')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with mecanicohasvehiculos
 */
class MecanicohasvehiculoController {
  /**
   * Show a list of all mecanicohasvehiculos.
   * GET mecanicohasvehiculos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let hasve = await mecanicoh.all()
        console.log(hasve.rows);
        return view.render('mecanicohasvehiculo/mecanicohasVehiculo',{hasvehiculo: hasve.rows})
  }

  /**
   * Render a form to be used for creating a new mecanicohasvehiculo.
   * GET mecanicohasvehiculos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new mecanicohasvehiculo.
   * POST mecanicohasvehiculos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single mecanicohasvehiculo.
   * GET mecanicohasvehiculos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing mecanicohasvehiculo.
   * GET mecanicohasvehiculos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update mecanicohasvehiculo details.
   * PUT or PATCH mecanicohasvehiculos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a mecanicohasvehiculo with id.
   * DELETE mecanicohasvehiculos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = MecanicohasvehiculoController
