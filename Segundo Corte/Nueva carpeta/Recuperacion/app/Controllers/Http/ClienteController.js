'use strict'//ESTE
const client = use('App/Models/Client')
const vehi = use('App/Models/Vehiculo')
class ClienteController {

    async index ({ request, response, view }) {
        let clients = await client.query().with('vehiculo').fetch();
        let vehic = await vehi.all()
        console.log(clients.toJSON());
        return view.render('clientes/cliente',{clients: clients.toJSON(),vehic:vehic.toJSON()})
    }
    
        async create ({ request, response, view }) {
            const add = new client();
            add.nombre = request.input('nombre')
            add.telefono = request.input('telefono')
            add.direccion = request.input('direccion')
        
            await add.save()
            return response.redirect('agregarCliente')
        }
      
  async view({request, response, view}){
    let cliente = await client.all();
    return view.render('clientes/agregarCliente',{cliente})
  }

  async store ({ request, response }) {
    console.log(request.all());

    await Cliente.save(request.all())
    return view.redirect('clientes/cliente')
  }

  async show ({ params, request, response, view }) {
    let cliente = await client.query().with('vehi').fetch()
    let vehi = await vehi.all();
    return view.render('cliente',{vehi:vehi.toJSON(),cliente:cliente.toJSON()})
  }

  async edit ({ params, request, response, view }) {//vista
    let clients = await client.find(params.id)    
    return view.render('clientes/modificarCliente',{clients})
  }


  async update ({ params, request, response,view }) {//metodo
      const clients = await client.find(params.id);
      clients.nombre = request.input('nombre')
      clients.telefono = request.input('telefono')
      clients.direccion = request.input('direccion')
      await clients.save()
      //return view.render('clientes/cliente',{clients})
      return response.redirect('/clientes/cliente')
  }

  async destroy ({ params, request, response,view }) {
    const cliente = await client.find(params.id);
    await cliente.delete()
    return response.redirect('back')
  }

}

module.exports = ClienteController