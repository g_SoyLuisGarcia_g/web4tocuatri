'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Vehiculo extends Model {
  
  clientes(){
    return this.belongsTo('App/Models/Client','id')
  }
}

module.exports = Vehiculo
