'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Repuesto extends Model {
    static get table (){
        return 'repuestos'
      }
}

module.exports = Repuesto
