'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Hoja extends Model {
    static get table (){
        return 'hojas'
      }
}

module.exports = Hoja
