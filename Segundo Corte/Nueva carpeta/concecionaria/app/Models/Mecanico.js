'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Mecanico extends Model {
    static get table (){
        return 'mecanico_responsables'
      }
      cliente(){
        return this.belongsTo('App/Models/Client')
      }
}

module.exports = Mecanico
