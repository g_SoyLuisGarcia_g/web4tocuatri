'use strict'//ESTE
const Mecanicos = use('App/Models/Mecanico')
const client = use('App/Models/client')
class MecanicoController {

    async index ({ request, response, view }) {
        let Mecanico = await Mecanicos.query().with('cliente').fetch();
        let clients = await client.all()
        console.log(Mecanico.toJSON());
        return view.render('mecanicoresponsable/mecanicoResponsable',{Mecanico: Mecanico.toJSON(),clients:clients.toJSON()})
    }
    
        async create ({ request, response, view }) {
            const add = new Mecanicos();
            add.nombre = request.input('nombre')
            add.direccion = request.input('direccion')
            add.tel = request.input('tel')
            add.costoxhora = request.input('costoxhora')
            add.categoria = request.input('categoria')
        
            await add.save()
            return response.redirect('agregarmecanicoResponsable')
        }
      
  async view({request, response, view}){
    let cliente = await Mecanicos.all();
    return view.render('mecanicoresponsable/agregarmecanicoResponsable',{cliente})
  }

  async store ({ request, response }) {
    console.log(request.all());

    await Cliente.save(request.all())
    return view.redirect('mecanicoresponsable/mecanicoResponsable')
  }

  async show ({ params, request, response, view }) {
    let cliente = await client.query().with('vehi').fetch()
    let vehi = await vehi.all();
    return view.render('mecanicoResponsable',{Mecanicos:Mecanicos.toJSON(),cliente:cliente.toJSON()})
  }

  async edit ({ params, request, response, view }) {//vista
    let clients = await client.find(params.id)    
    return view.render('mecanicoresponsable/modificarmecanicoResponsable',{clients})
  }


  async update ({ params, request, response,view }) {//metodo
      const clients = await client.find(params.id);
      clients.nombre = request.input('nombre')
      clients.direccion = request.input('direccion')
      clients.tel = request.input('telefono')
      clients.costoxhora = request.input('costo por hora')
      clients.categoria = request.input('categoria')
      await clients.save()
      //return view.render('clientes/cliente',{clients})
      return response.redirect('/mecanicoresponsable/mecanicoResponsable')
  }

  async destroy ({ params, request, response,view }) {
    const cliente = await client.find(params.id);
    await cliente.delete()
    return response.redirect('back')
  }

}

module.exports = MecanicoController