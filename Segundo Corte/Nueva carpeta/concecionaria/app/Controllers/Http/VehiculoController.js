'use strict'
const vehi = use('App/Models/Vehiculo')
const client = use('App/Models/Client')

class VehiculoController {

  async index ({ request, response, view }) {
    let vehiculo = await vehi.query().with('clientes').fetch()
    let cliente = await client.all()
    console.log(vehiculo.toJSON());
    return view.render('vehiculo/vehiculo',{vehiculos: vehiculo.toJSON(),cliente:cliente.toJSON()})
  }

  async show({params,response,request,view}){
    let vehic = await vehi.query().with('client').fetch()
      let cliente = await client.all()
     console.log("---------------------------------------------------------"+producto.toJSON())
    return view.render('vehiculo/agregarVehiculo',{cliente: cliente.toJSON(),vehic: vehic.toJSON()})
  }

  async create ({ request, response, view }) {
      const agre = new vehi();
      agre.modelo = request.input('modelo')
      agre.color = request.input('color')
      agre.fecha_ent = request.input('fecha_ent')
      agre.hora_ent = request.input('hora_ent')
      agre.cliente_idRFC = request.input('cliente_idRFC')
      await agre.save()
      return response.redirect('agregarVehiculo')

  }
  async store ({ request, response }) {
    console.log(request.all());
    await Vehiculo.save(request.all())
    return view.redirect('vehiculos/vehiculo')
  }
  
  /*async view({params,response,request,view}){
    let vehiculo = await vehi.find(params.id)    
    return view.render('/vehiculo/modificarVehiculo',{vehiculo})
  }*/
  async view({request, response, view}){
    let vehic = await vehi.query().with('clientes').fetch()
    return view.render('vehiculo/agregarVehiculo',{vehic:vehic.toJSON()})
  }
  
  async edit ({ params, request, response, view }) {//vista
    let vehiculo = await vehi.find(params.id)    
    return view.render('vehiculo/modificarVehiculo',{vehiculo})
  }


  async update({params,response,request,view}) {//metodo
      const upvehi = await vehi.find(params.id);
      upvehi.modelo = request.input('modelo')
      upvehi.color = request.input('color')
      upvehi.fecha_ent = request.input('fecha_ent')
      upvehi.hora = request.input('hora_ent')
      upvehi.cliente_idRFC = request.input('cliente_idRFC')
      await upvehi.save()
      //return view.render('clientes/cliente',{clients})
      return response.redirect('/vehiculo/vehiculo')
  }

  async destroy({params,response,request,view}) {
    const desvehiculo = await vehi.find(params.id);
    await desvehiculo.delete()
    return response.redirect('back')
  }

}

module.exports = VehiculoController