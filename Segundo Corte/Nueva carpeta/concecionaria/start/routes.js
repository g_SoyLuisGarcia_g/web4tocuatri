'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('/clientes/cliente')

//Rutas cliente
//Route.on('/').render('Menu')
Route.get('clientes/cliente','ClienteController.index')
Route.get('clientes/agregarCliente','ClienteController.view')
Route.post('clientes/agregarCliente','ClienteController.create')
Route.get('clientes/cliente/:id','ClienteController.destroy')//idcliente
Route.post('clientes/modificarCliente/:id','ClienteController.update')
Route.get('clientes/modificarCliente/:id','ClienteController.edit')

//Rutas vehiculo
Route.get('vehiculo/vehiculo','VehiculoController.index')
Route.get('vehiculo/agregarVehiculo','VehiculoController.view')
Route.post('vehiculo/agregarVehiculo','VehiculoController.create')
Route.get('vehiculo/vehiculo/:id','VehiculoController.destroy')
Route.get('vehiculo/modificarVehiculo/:id','VehiculoController.edit')
Route.post('vehiculo/modificarVehiculo/:id','VehiculoController.update')



//Rutas mecanico responsable
Route.get('mecanicoresponsable/mecanicoResponsable','MecanicoController.index')
Route.get('mecanicoresponsable/agregarmecanicoResponsable','MecanicoController.view')
Route.post('mecanicoresponsable/agregarmecanicoResponsable','MecanicoController.create')
Route.get('mecanicoresponsable/mecanicoResponsable/:id','MecanicoController.destroy')
Route.post('mecanicoresponsable/modificarmecanicoResponsable/;id','MecanicoController.update')
Route.get('mecanicoresponsable/modificarmecanicoResponsable/;id','MecanicoController.edit')

//Rutas factura
Route.get('factura/factura','FacturaController.index')
Route.get('factura/agregarfactura','FacturaController.view')
Route.post('factura/agregarfactura','FacturaController.create')
Route.get('factura/factura/:id','FacturaController.destroy')
Route.post('factura/modificarfactura/:id','FacturaController.update')
Route.get('factura/modificarfactura/:id','FacturaController.edit')
//Rutas hoja parte
Route.get('hoja/hojas','HojaController.index')
Route.get('hoja/agregarhojas','HojaController.view')
Route.post('hoja/agregarhojas','HojaController.create')
Route.get('hoja/hojas/:id','HojaController.destroy')
Route.post('hoja/modificarhojas/:id','HojaController.update')
Route.get('hoja/modificarhojas/:id','HojaController.edit')
//Ruta repuesto
Route.get('repuesto/repuesto','RepuestoController.index')
Route.get('repuesto/agregarrepuesto','RepuestoController.view')
Route.post('repuesto/agregarrepuesto','RepuestoController.create')
Route.get('repuesto/repuesto/:id','RepuestoController.destroy')
Route.post('repuesto/modificarrepuesto/:id','RepuestoController.update')
Route.get('repuesto/modificarrepuesto/:id','RepuestoController.edit')



