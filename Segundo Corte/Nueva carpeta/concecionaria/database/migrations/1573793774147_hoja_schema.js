'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HojaSchema extends Schema {
  up () {
    this.create('hojas', (table) => {
      table.increments('id')
      table.string('concepto',45)
      table.integer('cantidad')
      table.string('reparacion',45)
      table.integer('mecanico responsable_idMEC').unsigned().references('id').inTable('mecanico_responsables')
      table.timestamps()
    })
  }

  down () {
    this.drop('hojas')
  }
}

module.exports = HojaSchema
