'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehiculoSchema extends Schema {
  up () {
    this.create('vehiculos', (table) => {
      table.increments('id')
      table.string('modelo',45).notNullable()
      table.string('color',45).notNullable()
      table.datetime('fecha_ent').notNullable()
      table.time('hora_ent').notNullable()
      table.integer('cliente_idRFC').unsigned().references('id').inTable('clientes')
      table.timestamps()
    })
  }

  down () {
    this.drop('vehiculos')
  }
}

module.exports = VehiculoSchema
