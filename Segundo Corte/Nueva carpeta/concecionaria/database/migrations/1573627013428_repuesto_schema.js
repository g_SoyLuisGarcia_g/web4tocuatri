'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RepuestoSchema extends Schema {
  up () {
    this.create('repuestos', (table) => {
      table.increments('id')
      table.string('descripcion')
      table.integer('costounit')
      table.integer('preciounit')
      table.integer('hoja partes_idHOJA').unsigned().references('id').inTable('hojas')
      table.integer('hoja partes_MECANICO RESPONSABLE_idMEC').unsigned().references('mecanico responsable_idMEC').inTable('hojas')
      table.timestamps()
    })
  }

  down () {
    this.drop('repuestos')
  }
}

module.exports = RepuestoSchema
